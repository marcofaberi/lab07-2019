package it.unimi.di.se.lab07;

import it.unimi.di.se.lecture.Quackable;

import java.util.Observable;
import java.util.Observer;

public class QuackCounterObserver implements Observer {

    public static final String MALLARD_DUCK = "MallardDuck";
    public static final String GOOSE = "Goose";
    public static final String RED_HEAD_DUCK = "RedHeadDuck";
    public static final String RUBBER_DUCK = "RubberDuck";


    private final String[] istances= {MALLARD_DUCK, GOOSE, RED_HEAD_DUCK, RUBBER_DUCK};
    private final int[] vIstances= {0,0,0,0};

    @Override
    public void update(Observable observable, Object o) {
        update((ObservableQuacker)observable);
    }

    public void update(ObservableQuacker observalbe){
        Quackable q= observalbe.getQuackable();
        incrementIstance(q);
    }

    private void incrementIstance(Quackable q) {
        for(int i=0;i<istances.length;i++){
            if(q.getClass().getSimpleName().equals(istances[i]))
                vIstances[i]++;
        }
    }

    public int getQuackCount(final String name){
        int ret=0;
        for(int i=0;i<istances.length;i++){
            if(istances[i].equals(name))
                return vIstances[i];
        }
        return ret;
    }

    public void resetQuackCounts(){
        for(int i=0;i<vIstances.length;i++){
            vIstances[i]=0;
        }
    }
}
