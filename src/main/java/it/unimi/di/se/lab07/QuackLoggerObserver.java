package it.unimi.di.se.lab07;

import java.util.Observable;
import java.util.Observer;

public class QuackLoggerObserver implements Observer{

    @Override
    public void update(Observable observable, Object o) {
        System.err.println(o);
    }
}
