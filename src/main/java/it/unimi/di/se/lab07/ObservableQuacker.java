package it.unimi.di.se.lab07;

import it.unimi.di.se.lecture.Quackable;

import java.util.Observable;
import java.util.*;

public class ObservableQuacker extends Observable implements Quackable{
    private Quackable q;
    //private ArrayList<Observer> observers= new ArrayList<Observer>();

    public ObservableQuacker(Quackable q){
        this.q=q;
    }


    @Override
    public void quack() {
        q.quack();
        this.setChanged();
        this.notifyObservers(q.getClass().getSimpleName());
    }

    public Quackable getQuackable() {
        return q;
    }
}
