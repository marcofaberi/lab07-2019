package it.unimi.di.se.lab07;

import it.unimi.di.se.lecture.*;

import java.util.Observer;

public class ObservedDuckFactory extends AbstractDuckFactory {

    Observer observer=null;

    public ObservedDuckFactory(Observer o) {
        observer=o;
    }

    public Observer getObserver() {
        return observer;
    }

    @Override
    public Quackable createMallardDuck() {
        MallardDuck m= new MallardDuck();
        ObservableQuacker observeble= new ObservableQuacker(m);
        observeble.addObserver(observer);
        return m;
    }

    @Override
    public Quackable createRedHeadDuck() {
        RedHeadDuck r=new RedHeadDuck();
        ObservableQuacker observeble= new ObservableQuacker(r);
        return r;
    }

    @Override
    public Quackable createRubberDuck() {
        RubberDuck r=new RubberDuck();
        ObservableQuacker observeble= new ObservableQuacker(r);
        return r;
    }

    @Override
    public Quackable createQuackableGoose() {
        QuackableGoose g=new QuackableGoose(new Goose());
        ObservableQuacker observeble= new ObservableQuacker(g);
        return g;
    }

    @Override
    public Quackable createQuackableHonkableGoose() {
        QuackableHonkableGoose h=new QuackableHonkableGoose();
        ObservableQuacker observeble= new ObservableQuacker(h);
        return h;
    }
}
