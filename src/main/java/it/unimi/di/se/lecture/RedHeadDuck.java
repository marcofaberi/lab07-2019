package it.unimi.di.se.lecture;

public class RedHeadDuck implements Quackable {

	@Override
	public void quack() {
		System.out.println("quack");
	}
}
