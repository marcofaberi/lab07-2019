package it.unimi.di.se.lecture;

public class QuackCounter implements Quackable {

	private static int counter = 0;
	private final Quackable quacker;

	public QuackCounter(Quackable quacker) {
		this.quacker = quacker;
	}

	@Override
	public void quack() {
		quacker.quack();
		counter++;
	}

	public static int getQuackNumber() {
		return counter;
	}

	public static void reset() {
		counter = 0;
	}
}
