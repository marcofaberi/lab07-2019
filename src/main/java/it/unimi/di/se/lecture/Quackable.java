package it.unimi.di.se.lecture;

public interface Quackable {

	void quack();
}
