package it.unimi.di.se.lecture;

public class CounterDuckFactory extends AbstractDuckFactory {

	@Override
	public Quackable createMallardDuck() {
		return new QuackCounter(new MallardDuck());
	}

	@Override
	public Quackable createRedHeadDuck() {
		return new QuackCounter(new RedHeadDuck());
	}

	@Override
	public Quackable createRubberDuck() {
		return new QuackCounter(new RubberDuck());
	}

	@Override
	public Quackable createQuackableGoose() {
		return new QuackCounter(new QuackableGoose(new Goose()));
	}

	@Override
	public Quackable createQuackableHonkableGoose() {
		return new QuackCounter(new QuackableHonkableGoose());
	}
}
