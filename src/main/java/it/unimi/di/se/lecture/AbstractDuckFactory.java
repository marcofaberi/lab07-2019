package it.unimi.di.se.lecture;

public abstract class AbstractDuckFactory {

	abstract public Quackable createMallardDuck();

	abstract public Quackable createRedHeadDuck();

	abstract public Quackable createRubberDuck();

	abstract public Quackable createQuackableGoose();

	abstract public Quackable createQuackableHonkableGoose();
}
