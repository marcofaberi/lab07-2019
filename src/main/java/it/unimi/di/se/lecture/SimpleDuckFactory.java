package it.unimi.di.se.lecture;

public class SimpleDuckFactory extends AbstractDuckFactory {

	@Override
	public Quackable createMallardDuck() {
		return new MallardDuck();
	}

	@Override
	public Quackable createRedHeadDuck() {
		return new RedHeadDuck();
	}

	@Override
	public Quackable createRubberDuck() {
		return new RubberDuck();
	}

	@Override
	public Quackable createQuackableGoose() {
		return new QuackableGoose(new Goose());
	}

	@Override
	public Quackable createQuackableHonkableGoose() {
		return new QuackableHonkableGoose();
	}
}
