package it.unimi.di.se.lecture;

public class QuackableHonkableGoose extends Goose implements Quackable {

	@Override
	public void quack() {
		honk();
	}
}
