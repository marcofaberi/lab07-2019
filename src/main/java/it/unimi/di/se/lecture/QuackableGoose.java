package it.unimi.di.se.lecture;

public class QuackableGoose implements Quackable {

	private final Goose goose;

	public QuackableGoose(Goose goose) {
		this.goose = goose;
	}

	@Override
	public void quack() {
		goose.honk();
	}
}
