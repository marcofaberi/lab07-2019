package it.unimi.di.se.lab07;

import it.unimi.di.se.lecture.MallardDuck;
import it.unimi.di.se.lecture.QuackCounter;
import it.unimi.di.se.lecture.Quackable;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemErrRule;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.rules.Timeout;

import java.util.Observer;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class TestQuackLoggerObserver {

	public static final String QUACK = "quack\n";
	public static final String OBSERVABLE_QUACKER = "ObservableQuacker\n";
	public static final String MALLARD_DUCK = "MallardDuck";
	@Rule
	public Timeout globalTimeout = Timeout.seconds(2); // 2 seconds max per test

	@Rule
	public final SystemErrRule stderr = new SystemErrRule().enableLog();
	@Rule
	public final SystemOutRule output = new SystemOutRule().enableLog();

	private QuackLoggerObserver logger;

	@Before
	public void setUp() {
		logger = new QuackLoggerObserver();
	}

	@Test
	public void test01(){
		ObservableQuacker o= new ObservableQuacker(new MallardDuck());
		o.quack();
		assertThat(output.getLog()).isEqualTo(QUACK);
		o.addObserver(logger);
		o.quack();
		assertThat(output.getLog()).isEqualTo(QUACK+QUACK);
	}

	@Test
	public void test02(){
		ObservableQuacker o= new ObservableQuacker(new MallardDuck());
		o.addObserver(logger);
		o.quack();
		assertThat(stderr.getLog()).isEqualTo(MALLARD_DUCK+"\n");
	}

	@Test
	public void test03(){
		QuackCounterObserver quackobserver= new QuackCounterObserver();
		ObservableQuacker o= new ObservableQuacker(new MallardDuck());
		o.addObserver(quackobserver);
		o.quack();
		assertThat(quackobserver.getQuackCount(MALLARD_DUCK)).isEqualTo(1);
		quackobserver.resetQuackCounts();
		assertThat(quackobserver.getQuackCount(MALLARD_DUCK)).isEqualTo(0);
	}

	@Test
	public void test04(){
		QuackCounterObserver quackobserver= new QuackCounterObserver();
		ObservedDuckFactory duckFactory= new ObservedDuckFactory(quackobserver);
		Quackable m=duckFactory.createMallardDuck();
		m.quack();
		Observer ob=duckFactory.getObserver();
		assertThat(output.getLog()).isEqualTo(QUACK);
	}

}
