package it.unimi.di.se.lecture;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

import it.unimi.di.se.lecture.AbstractDuckFactory;
import it.unimi.di.se.lecture.CounterDuckFactory;
import it.unimi.di.se.lecture.Flock;
import it.unimi.di.se.lecture.Goose;
import it.unimi.di.se.lecture.MallardDuck;
import it.unimi.di.se.lecture.QuackCounter;
import it.unimi.di.se.lecture.Quackable;
import it.unimi.di.se.lecture.RedHeadDuck;
import it.unimi.di.se.lecture.RubberDuck;
import it.unimi.di.se.lecture.SimpleDuckFactory;

public class TestDuck {

	@Rule
	public final SystemOutRule output = new SystemOutRule().enableLog();

	@Before
	public void resetCounter() {
		QuackCounter.reset();
	}

	@Test
	public void mallardDuckQuacks() {
		Quackable duck = new MallardDuck();
		duck.quack();
		assertEquals(output.getLog(), "quack\n");
	}

	@Test
	public void otherDucksQuack() throws Exception {
		Quackable rubby = new RubberDuck();
		Quackable reddy = new RedHeadDuck();
		rubby.quack();
		reddy.quack();
		assertEquals(output.getLog(), "squeak\nquack\n");
	}

	@Test
	public void gooseHonks() throws Exception {
		Goose goose = new Goose();
		goose.honk();
		assertEquals(output.getLog(), "honk\n");
	}

	private Flock setupAMixedFlock(AbstractDuckFactory factory) {
		Flock flock = new Flock();
		flock.add(factory.createMallardDuck());
		flock.add(factory.createRedHeadDuck());
		flock.add(factory.createRubberDuck());
		flock.add(factory.createQuackableGoose());
		flock.add(factory.createQuackableHonkableGoose());
		return flock;
	}

	@Test
	public void aFlockQuacks() throws Exception {
		AbstractDuckFactory factory = new SimpleDuckFactory();
		Flock flock = setupAMixedFlock(factory);
		flock.quack();
		assertEquals(output.getLog(), "quack\nquack\nsqueak\nhonk\nhonk\n");
	}

	@Test
	public void countQuacks() throws Exception {
		AbstractDuckFactory factory = new CounterDuckFactory();
		Flock flock = setupAMixedFlock(factory);
		flock.quack();
		assertEquals(QuackCounter.getQuackNumber(), 5);
	}

	private Flock setupNumberedFlock(AbstractDuckFactory fact, int n) {
		Flock flock = new Flock();
		for (int i = 0; i < n; i++)
			flock.add(fact.createRedHeadDuck());
		return flock;
	}

	@Test
	public void aFlockIsQuackable() throws Exception {
		AbstractDuckFactory factory = new CounterDuckFactory();
		Flock quackableFlock = setupNumberedFlock(factory, 10);
		Flock flock = setupNumberedFlock(factory, 5);
		flock.add(quackableFlock);
		flock.quack();
		assertEquals(QuackCounter.getQuackNumber(), 15);
	}

}
