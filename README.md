# Corso di Ingegneria del Software a.a. 2017/18

## Laboratorio 7

Ogni coppia di studenti procede ad effettuare il **fork** di questo repository.
Inoltre, concede i permessi di scrittura al proprio compagno di team e i **permessi di lettura** ai due docenti (`carlobellettini` e `matteocamilli`).

Una volta effettuato il **clone** del repository, il **driver** effettua `cd` nella directory del progetto ed esegue il comando:

```
bash start.sh email_unimi  username_bitbucket username_bitbucket_compagno
```

- controllare con il nuovo comando `git pair` che gli username siano corretti
- ogni volta che si cambia chi ha la tastiera (normalmente dopo aver fatto passare un nuovo test),
  dare il comando `git pair swap` e controllare con `git pair` che l'ordine delle username sia corretto

## Processo

Una volta effettuato il **clone** del repository, il gruppo esegue il comando `git flow init` all'interno della directory clonata.
Dopodiché, il gruppo implementa secondo la *metodologia TDD*
le specifiche riportate di seguito; in maggior dettaglio, ripete i passi seguenti fino ad aver implementato tutte le funzionalità richieste:

* crea un nuovo *branch* per la funzionalità corrente attraverso l'esecuzione del comando `git flow feature start`,
* implementa un test per le funzionalità richieste **procedendo nell'ordine** in cui sono specificate,
* verifica che **il codice compili correttamente**, ma l'**esecuzione del test fallisca**; solo a questo punto effettua un *commit* (usando `git add` e `git commit`) iniziando il messaggio di commit con la stringa `ROSSO:`,
* aggiunge la minima implementazione necessaria a realizzare la funzionalità, in modo che **il test esegua con successo**; solo a questo punto
  effettua un *commit* (usando `git add` e `git commit`) iniziando il messaggio di commit con la stringa `VERDE:`,
* procede, se necessario, al **refactoring** del codice, accertandosi che le modifiche non comportino il fallimento di alcun test; solo in questo caso fa seguire ad ogni
  passo un *commit* (usando `git add` e `git commit`) iniziando il messaggio di commit con la stringa `REFACTORING:`,
* esegue il *merge* del *branch* per la funzionalità sviluppata all'interno del *branch develop* attraverso il comando `git flow feature finish`,
* **solo in fase di rilascio**, esegue una *release* all'interno del *branch master* attraverso il comando `git flow release start` e successivamente `git flow release finish`,
* effettua un *push* (di tutti i *branch*) su Bitbucket con `git push origin --all --follow-tags`.

Al termine del laboratorio effettua una **ultima release**, un ultimo *push*, e **verifica su Bitbucket** che ci sia la completa traccia di *commit* effettuati.
Si suggerisce di eseguire i test non soltanto con Eclipse, ma anche eseguendo `./gradlew test` da riga di comando.


### Specifica dei requisiti

Obiettivo dell'esercizio è *progettare* e *realizzare* un insieme di classi
che consentano di:

* **contare** il numero di `quack` per *specie* di anatra (ossia per ogni
  nome di classe che implementa l'interfaccia [Quackable](/src/main/java/it/unimi/di/se/lecture/Quackable.java)),
* **emettere su _standard error_** un messaggio di *log* contenente il nome
  della *specie*  per ogni `quack`.

Tale obiettivo deve essere raggiunto usando il *design pattern* denominato
**_observer_**; più in dettaglio, è richiesta la realizzazione, nel *package*
`it.unimi.di.se.lab07`, delle seguenti classi:

1. il **_decorator_** `ObservableQuacker` che implementi l'interfaccia [Quackable](/src/main/java/it/unimi/di/se/lecture/Quackable.java)
   ed estenda la classe [Observable](https://docs.oracle.com/javase/8/docs/api/java/util/Observable.html) e decori il [Quackable](/src/main/java/it/unimi/di/se/lecture/Quackable.java)
   passato al suo costruttore in modo che l'invocazione del metodo `quack` possa essere osservata;

1. l'**_observer_** `QuackLoggerObserver` in modalità **push** che implementi l'interfaccia [Observer](https://docs.oracle.com/javase/8/docs/api/java/util/Observer.html)
   ed emetta sullo _standard error_, per ogni invocazione di `quack`, il nome della
   classe sulla cui istanza osservata è stato invocato;

1. l'**_observer_** `QuackCounterObserver` in modalità **pull** che implementi l'interfaccia [Observer](https://docs.oracle.com/javase/8/docs/api/java/util/Observer.html)
   ed abbia un metodo con segnatura `public int getQuackCount(final String name)`
   che, dato un nome di classe come argomento, restituisca il numero di volte
   per cui `quack` è stato invocato su istanze osservate di tale classe e un metodo con segnatura
   `public void resetQuackCounts()` che azzeri tutti i conteggi;

1. la **_factory_** `ObservedDuckFactory` che implementi la classe astratta [AbstractDuckFactory](/src/main/java/it/unimi/di/se/lecture/AbstractDuckFactory.java)
   e il cui costruttore accetti una istanza di uno dei due [Observer](https://docs.oracle.com/javase/8/docs/api/java/util/Observer.html)
   di cui sopra e costruisca le istanze di vari tipi di *duck* opportunamente
   avvolte dal decorator `ObservableQuacker` e poste sotto osservazione dall'*observer* passato al
   costruttore.


#### Suggerimenti

Nell'implementazioni delle classi sopra specificate può essere utile tenere
conto dei seguenti suggerimenti:

1. la classe `ObservableQuacker` è bene che decori il metodo `quack` usando almeno
   i metodi [setChanged](https://docs.oracle.com/javase/8/docs/api/java/util/Observable.html#setChanged--)
   e [notifyObservers](https://docs.oracle.com/javase/8/docs/api/java/util/Observable.html#notifyObservers-java.lang.Object-)
   necessari a rendere le invocazioni osservabili dai due *observer* da implementare;

1. la classe di cui un oggetto è istanza può essere ottenuta invocando
   [getClass](https://docs.oracle.com/javase/8/docs/api/java/lang/Object.html#getClass--),
   data una classe è possibile ottenere il suo nome invocando
   [getSimpleName](https://docs.oracle.com/javase/8/docs/api/java/lang/Class.html#getSimpleName--);
   detto altrimenti, dato un oggetto definito come `AClass anObject = new AClass()`, l'espressione
   `anObject.getClass().getSimpleName()` ha valore `AClass`;

1. la classe `QuackLoggerObserver` emetterà i messaggi invocando opportuni metodi di [System.err](https://docs.oracle.com/javase/8/docs/api/java/lang/System.html#err),
   per testare il suo comportamento si può usare una
   [SystemErrRule](https://stefanbirkner.github.io/system-rules/apidocs/org/junit/contrib/java/lang/system/SystemErrRule.html)
   in analogia a quanto fatto in [TestDuck](/src/test/java/it/unimi/di/se/lecture/TestDuck.java) per il testing dei
   messaggi emessi su _standard output_.

1. la classe `QuackCounterObserver` può usare una implementazione di [Map](https://docs.oracle.com/javase/8/docs/api/java/util/Map.html)
   (di tipo specifico `Map<String,Integer>`) per tener traccia del numero di invocazioni per classe;

1. si ricordi che, al fine di rendere osservabile un [Observable](https://docs.oracle.com/javase/8/docs/api/java/util/Observable.html)
   da parte del suo [Observer](https://docs.oracle.com/javase/8/docs/api/java/util/Observer.html), è necessario
   invocare il metodo [addObserver](https://docs.oracle.com/javase/8/docs/api/java/util/Observable.html#addObserver-java.util.Observer-).

### Verifica tramite QA Test

Si osservi che le classi implementate dovranno **rispettare strettamente le
specifiche** sopra elencate, in particolare riguardo a: il *package* in cui
devono essere collocate, il nome che devono avere, i parametri dei costruttori
e le segnature dei metodi addizionali.

Al fine di verificare tale richiesta, nonché la corretta implementazione dei
due *observer*, sono stati realizzati dei *test di accettazione* da parte del
team di QA messi a disposizione in due *branch* di questo repository sotto
forma di due classi di test JUnit (una per ciascun *observer*).

Una volta implementato il codice, il gruppo può aggiungere i file di test per
`QuackLoggerObserver` con il comando
```
git merge origin/qa_logger -m'QA Logger'
```
e quelli per `QuackCounterObserver` con il comando
```
git merge origin/qa_counter -m'QA Counter'
```
ed eseguire i test da riga di comando invocando `./gradlew test`.
